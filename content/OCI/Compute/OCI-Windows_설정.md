# OCI-Windows 설정

## 1. 생성 시 패스워드 설정

### init 를 이용한 패스워드 설정

- 인스턴스 생성 시 init 에 다음과 같이 패스워드를 입력 후 생성

```bash
<powershell>
net user opc "PassWord12#$"

restart-computer
</powershell>
```

- 인스턴스 기동 후 재 기동  하면 패스워드가 변경 된 걸로 로그인 가능

---

## 2. 한글 설정 방법

1. **윈도우 오른쪽 버튼 클릭하여 Settings를 클릭 합니다.**

   ![Untitled](../../OCI/Compute/images/OCI-Windows_설정/1.png)



2. **Time & Language 를 클릭 합니다**

![Untitled](../../OCI/Compute/images/OCI-Windows_설정/2.png)

3. **Region 변경** 

![Untitled](../../OCI/Compute/images/OCI-Windows_설정/3.png)

4. **Language 에서 +Add a language를 클릭 합니다**

![Untitled](../../OCI/Compute/images/OCI-Windows_설정/4.png)

![Untitled](../../OCI/Compute/images/OCI-Windows_설정/5.png)

![Untitled](../../OCI/Compute/images/OCI-Windows_설정/6.png)

5. **재 기동 후 한글로 변경 된 것을  확인  합니다.**

![Untitled](../../OCI/Compute/images/OCI-Windows_설정/7.png)

## 3. 표준 시간대 설정

1. 윈도우 버튼을 클릭 후 서버 관리자를 클릭 합니다. 

![Untitled](../../OCI/Compute/images/OCI-Windows_설정/8.png)

2. 로컬 서버 → 표준 시간대를 클릭 합니다

![Untitled](../../OCI/Compute/images/OCI-Windows_설정/9.png)

3. 표준 시간대 변경을 클릭 후 서울로 변경 후 재부팅 진행합니다.

![Untitled](../../OCI/Compute/images/OCI-Windows_설정/10.png)

![Untitled](.\images\OCI-Windows_설정/11.png)

## 4. NTP 설정

- PowerShell 을 관리자로 실행 합니다

```bash
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Services\W32Time\Parameters' -Name 'Type' -Value NTP -Type String
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Services\W32Time\Config' -Name 'AnnounceFlags' -Value 5 -Type DWord
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Services\W32Time\TimeProviders\NtpServer' -Name 'Enabled' -Value 1 -Type DWord
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Services\W32Time\Parameters' -Name 'NtpServer' -Value '169.254.169.254,0x9' -Type String
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Services\W32Time\TimeProviders\NtpClient' -Name 'SpecialPollInterval' -Value 900 -Type DWord
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Services\W32Time\Config' -Name 'MaxPosPhaseCorrection' -Value 1800 -Type DWord
Set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Services\W32Time\Config' -Name 'MaxNegPhaseCorrection' -Value 1800 -Type DWord

```

- ntp 연결 테스트
    - 폴링 간격으로 지정된 시간이 경과하면 State가 Pending에서 Active로 변경됩니다

```bash
PS C:\Windows\system32> w32tm /query /peers
#피어: 1

Peer: 169.254.169.254,0x9
State: Active
Time Remaining: 435.4108804s
Mode: 3 (Client)
Stratum: 2 (secondary reference - syncd by (S)NTP)
PeerPoll Interval: 6 (64s)
HostPoll Interval: 6 (64s)
```

## 5. SAC 활성화

- power shell을 관리자 계정으로 실행합니다.

```bash
# 직렬 콘솔 활성화 
PS C:\Windows\system32> bcdedit /ems '{current}' on
The operation completed successfully.

PS C:\Windows\system32> bcdedit /emssettings EMSPORT:1 EMSBAUDRATE:115200
The operation completed successfully.

# Windows 부팅 메뉴 활성화
PS C:\Windows\system32> bcdedit /set '{bootmgr}' displaybootmenu yes
The operation completed successfully.

PS C:\Windows\system32> bcdedit /set '{bootmgr}' timeout 15
The operation completed successfully.

PS C:\Windows\system32> bcdedit /set '{bootmgr}' bootems yes
The operation completed successfully.

# 인스턴스 재기동 
PS C:\Windows\system32> shutdown -r -t 0
```

- 유저 추가 및 그룹 설정

```bash
# 유저 추가
# net user msp <패스워드> /add

PS C:\Windows\system32> net user msp "PassWord12#$" /add
The command completed successfully.

# 유저 확인 
PS C:\Windows\system32> net user

User accounts for \\WIN-TEST

-------------------------------------------------------------------------------
Administrator            cloudbase-init           DefaultAccount
Guest                    msp                      opc
sshd                     WDAGUtilityAccount
The command completed successfully.

# administrators에 그룹에 추가
PS C:\Windows\system32> net localgroup administrators msp /add
The command completed successfully.

# administrators 그룹 확인

PS C:\Windows\system32> net localgroup administrators
Alias name     administrators
Comment        Administrators have complete and unrestricted access to the computer/domain

Members

-------------------------------------------------------------------------------
Administrator
cloudbase-init
msp
NT SERVICE\OCABSM
NT SERVICE\OCAOSMS
NT SERVICE\OCAU
NT SERVICE\OCAUM
NT SERVICE\OCAVSS
opc
The command completed successfully.
```

- 콘솔 연결

![Untitled](../../OCI/Compute/images/OCI-Windows_설정/12.png)

- 실행과정

```bash
Welcome to Oracle Cloud Shell.

Update: Cloud Shell will now use Oracle JDK 11 by default. To change this, see Managing Language Runtimes in the Cloud Shell documentation.

Your Cloud Shell machine comes with 5GB of storage for your home directory. Your Cloud Shell (machine and home directory) are located in: South Korea Central (Seoul).
You are using Cloud Shell in tenancy uclopn as an OCI Federated user oracleidentitycloudservice/bokim@uclick.co.kr

Type `help` for more info.
Running instance console connection version 1
Instance type is x86_64
Running from 1 folder
2023/01/19 08:00:13 Fetching instance info.
2023/01/19 08:00:13 Looking for existing console connections.
2023/01/19 08:00:16 Waiting for Instance Console Connection to complete transition. Current state: CREATING
2023/01/19 08:00:19 Waiting for Instance Console Connection to complete transition. Current state: CREATING
2023/01/19 08:00:22 Waiting for Instance Console Connection to complete transition. Current state: CREATING
2023/01/19 08:00:25 Instance Console Connection reached state: ACTIVE
2023/01/19 08:00:25 
=================================================
IMPORTANT: Use a console connection to troubleshoot a malfunctioning instance. For normal operations, you should connect to the instance using a Secure Shell (SSH) or Remote Desktop connection. For steps, see https://docs.cloud.oracle.com/iaas/Content/Compute/Tasks/accessinginstance.htm

For more information about troubleshooting your instance using a console connection, see the documentation: https://docs.cloud.oracle.com/en-us/iaas/Content/Compute/References/serialconsole.htm#four
=================================================

# CMD 시작
SAC>cmd
The Command Prompt session was successfully launched.

# CMD 전환 ( 탭전환 ) 
EVENT:   A new channel has been created.  Use "ch -?" for channel help.
Channel: Cmd0001

Name:                  Cmd0001
Description:           Command
Type:                  VT-UTF8
Channel GUID:          ce75b292-97cd-11ed-b5b3-020017008060
Application Type GUID: 63d02271-8aa4-11d5-bccf-00b0d014a2d0

Press <esc><tab> for next channel.
Press <esc><tab>0 to return to the SAC channel.
Use any other key to view this channel.

# 유저 로그인
Please enter login credentials.
Username: msp
Domain  :  ( 생략 )
Password: ********

# 로그인 완료 
Microsoft Windows [Version 10.0.17763.3650]                                     
(c) 2018 Microsoft Corporation. All rights reserved.                            
                                                                                
C:\Windows\system32> 

# 로그인 사용자 확인 
C:\Windows\system32>whoami                                                      
win-test\msp

# 파워쉘 전환
C:\Windows\system32>PowerShell                                                  
Windows PowerShell                                                              
Copyright (C) Microsoft Corporation. All rights reserved.
```

- 실행 화면

![Untitled](../../OCI/Compute/images/OCI-Windows_설정/13.png)



## 6. Custom Image Agent

- 2012 ~ 2019 범용 

[OracleCloudAgentSetup_v1.28.0.msi](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/bc41c00f-a47a-46cf-9e72-76fd056c13bf/OracleCloudAgentSetup_v1.28.0.msi)